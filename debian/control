Source: kvirc
Build-Depends:
 cmake (>= 3.1.0),
 debhelper-compat (= 13),
 doxygen,
 extra-cmake-modules,
 graphviz,
 libenchant-2-dev,
 libkf5coreaddons-dev,
 libkf5i18n-dev,
 libkf5notifications-dev,
 libkf5service-dev,
 libkf5windowsystem-dev,
 libkf5xmlgui-dev,
 libperl-dev,
 libphonon4qt5-dev,
 libqt5svg5-dev,
 libqt5webkit5-dev,
 libqt5x11extras5-dev,
 libssl-dev,
 libx11-dev,
 libxrender-dev,
 libxss-dev,
 pkg-config,
 pkg-kde-tools,
 python3-dev,
 qtmultimedia5-dev,
 zlib1g-dev,
Section: net
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Raúl Sánchez Siles <rasasi78@gmail.com>,
 Kai Wasserbäch <curan@debian.org>,
 Mark Purcell <msp@debian.org>,
 Andrey Rahmatullin <wrar@debian.org>
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/kvirc.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/kvirc
Homepage: https://www.kvirc.net/
Rules-Requires-Root: no

Package: kvirc
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: libkvilib5 (= ${binary:Version}),
 kvirc-modules (= ${binary:Version}),
 kvirc-data (= ${source:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Multi-Arch: foreign
Description: KDE-based next generation IRC client with module support
 A highly configurable graphical IRC client with an MDI interface,
 built-in scripting language, support for IRC DCC, drag & drop file
 browsing, and much more. KVIrc uses the KDE widget set, can be extended
 using its own scripting language, integrates with KDE, and supports
 custom plugins.
 .
 If you're looking for a simple and plain IRC client, KVIrc is probably the
 wrong choice as it is rather big. But if you want a highly customizable client
 you won't regret the installation.

Package: libkvilib5
Architecture: any
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: kvirc (= ${binary:Version})
Multi-Arch: same
Description: KVIrc (IRC client) base library
 A highly configurable graphical IRC client with an MDI interface,
 built-in scripting language, support for IRC DCC, drag & drop file
 browsing, and much more. KVIrc uses the KDE widget set, can be extended
 using its own scripting language, integrates with KDE, and supports
 custom plugins.
 .
 This package contains the main library of KVIrc.

Package: kvirc-modules
Architecture: any
Section: libs
Pre-Depends: ${misc:Pre-Depends}
Depends: libkvilib5 (= ${binary:Version}),
 ${shlibs:Depends},
 ${misc:Depends}
Recommends: kvirc (= ${binary:Version})
Multi-Arch: same
Description: KVIrc (IRC client) modules
 A highly configurable graphical IRC client with an MDI interface,
 built-in scripting language, support for IRC DCC, drag & drop file
 browsing, and much more. KVIrc uses the KDE widget set, can be extended
 using its own scripting language, integrates with KDE, and supports
 custom plugins.
 .
 This package contains all modules for KVIrc.

Package: kvirc-data
Architecture: all
Depends: ${misc:Depends}
Recommends: kvirc (>= ${source:Version})
Multi-Arch: foreign
Description: Data files for KVIrc
 This package contains the architecture-independent data needed by KVIrc in
 order to run, such as icons and images, language files, and shell scripts.
 It also contains complete reference guides on scripting and functions
 within KVIrc in its internal help format.
